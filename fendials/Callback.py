import dolfin
import numpy
from scipy.sparse.linalg import spsolve
from scipy.sparse.linalg import splu

class Callback(object):
    """
    Callbacks to feed SUNDIALS with information from DOLFIN
    """
    def __init__(self, u, F, fs, u0, bcs, dae, solver, ks, verbose=False):
        """
        u  : DOLFIN Function object, i.e. Function(V)
        F  : UFL form of the right-hand-side (or the ODE part), i.e. dot(u,v)*dx
        fs : array of time-dependent expressions, i.e. [Expression("2.0*x[0]*t"), t=0.0]
        u0 : initial condition, either an interplolated u or a DOLFIN expression is OK
        bcs: array of DirichletBCs, i.e. [DirichletBC(V, Constant('1.0'), boundary)]
        dae: array of indices of the differential part in a mixed function space
             In a single-space case, it's [0]; In a multi-space case (W=V*Q),
             if both parts are differential it's [0,1].
        solver: 'SGPMR' or 'DENSE'
        ks : array of parameter-dependent expressions
        verbose: signal to print some extra info for debug purpose, False by default
        """
        V = u.function_space()
        self.fs = fs
        self.ks = ks
        self.u0 = u0
        self.F = F
        self.bcs = bcs
        self.u = u
        self.jacobian = None
        self.preconditioner = None
        self.alpha = None
        self.numjac = 0
        self.dae = dae
        self.solver = solver
        if len(self.u.split()) == 0:
            self.shape = 1
        else:
            self.shape = len(self.u.split())
        num_of_ent = V.dofmap().num_entity_dofs(0)
        v_d = dolfin.vertex_to_dof_map(V)

        self.sub_vertex_to_dof_map = numpy.sort([[v_d[vi_n*num_of_ent + sub]
                                    for vi_n in range(V.mesh().num_vertices())]
                                   for sub in range(num_of_ent)])
        self.verbose = verbose
        self.stats = {'jac': 0, 'jacv': 0, 'prec_setup': 0, 'prec_solve': 0}

        if False: # check changes manually
            rhs = dolfin.assemble(self.F)
            rhs[:] = 0.0
            for bc in self.bcs:
                boundary = bc.user_sub_domain()
                subspace = bc.function_space()
                bc_rhs = dolfin.DirichletBC(subspace, dolfin.Constant(1.0), boundary)
                bc_rhs.apply(rhs)
            rhs_array = rhs.array()

            self.isfixed_mask = numpy.asarray(numpy.where(rhs_array > 0))[0]
            self.isfree_mask = numpy.asarray(numpy.where(rhs_array == 0))[0]
        else: # use get_boundary_values() method
            dof_isfixed = []
            dof_all = range(self.u.vector().size())
            for bc in self.bcs:
                dof_isfixed.extend(bc.get_boundary_values().keys())
            dof_isfree = list(set(dof_all) - set(numpy.asarray(dof_isfixed)))
            self.isfixed_mask = numpy.asarray(dof_isfixed, dtype=int)
            self.isfree_mask = numpy.asarray(dof_isfree, dtype=int)

        W = self.u.function_space()
        w = dolfin.TestFunctions(W)
        omg = dolfin.TrialFunctions(W)
        m = 0

        for i in self.dae:
            m += w[i]*omg[i]*dolfin.dx
        self.M = dolfin.assemble(m)

        du = dolfin.TrialFunction(V)
        self.J = dolfin.derivative(F, u, du)

        for row in self.isfixed_mask:
            self.M.zero(numpy.array([row], dtype=numpy.intc))

    def slopes(self, udot2, t0=0.0):
        '''
        to manually calculated consistent initial conditions,
        which is suppressed for now
        '''
        V = self.u.function_space()
        # self.update_time(t0)
        self.u0.t = t0
        u_0 = dolfin.interpolate(self.u0, V)

        MM = dolfin.as_backend_type(self.M).sparray()
        M11 = MM[self.isfree_mask, :][:, self.isfree_mask]
        M12 = MM[self.isfree_mask, :][:, self.isfixed_mask]

        self.u.assign(u_0)
        Fu0 = dolfin.assemble(self.F).array()

        b = Fu0[self.isfree_mask] - M12.dot(udot2)
        udot1 = spsolve(M11, b)

        udot_0 = numpy.ones(self.u.vector().size())
        udot_0[self.isfree_mask] = udot1
        udot_0[self.isfixed_mask] = udot2

        return udot_0

    def res(self, t_ida, u_ida, udot_ida, p=None):
        '''
        to evaluate residual
        '''
        if self.verbose:
            print '*-----res(e)'
            print '      at t =', t_ida
        self.update_time(t_ida)
        for i in range(len(self.ks)):
            self.ks[i].p = p[i]

        rhs = dolfin.assemble(self.F)
        rhs[:] = 0.0
        for bc in self.bcs:
            bc.apply(rhs)
        g = rhs.array()
        self.u.vector()[:] = u_ida
        b = dolfin.assemble(self.F)  

        residual = 0*b
        udot = 0*b
        udot[:] = udot_ida
        self.M.mult(udot, residual) 
        residual -= b

        for i in self.isfixed_mask:
            residual[i] = g[i] - u_ida[i]
        if self.verbose:
            print '*-----res(l)'
        return residual.array()

    def jac(self, alpha, t_ida, u_ida, udot_ida_unused, p=None):
        '''
        to evaluate Jacobian for direct solver
        '''
        self.stats['jac'] += 1
        if self.verbose:
            print '*----jac(e)'
            print '     at t = ', t_ida
            print '  1/alpha = ', 1.0/alpha
        self.update_time(t_ida)
        for i in range(len(self.ks)):
            self.ks[i].p = p[i]
        self.u.vector()[:] = u_ida
        A = dolfin.assemble(self.J)
        for bc in self.bcs:
            bc.apply(A)

        self.alpha = alpha
        self.jacobian = alpha*self.M - A
        self.numjac += 1
        if self.verbose:
            print '*----jac(l)'
        if self.solver == 'DENSE':
            return dolfin.as_backend_type(self.jacobian).array()
        else:
            return dolfin.as_backend_type(self.jacobian).sparray().tocsc()
   
    def jacv(self, t, u, udot, res, v, alpha, p=None):
        '''
        to evaluate JacobianV for SPGMR solver
        '''
        if self.verbose:
            print '*---jacv(e)'
            print '    at t =', t
        self.stats['jacv'] += 1
        for i in range(len(self.ks)):
            self.ks[i].p = p[i]
        jacdotv = self.jac(alpha, t, u, udot, p).dot(v)
        if self.verbose:
            print '*---jacv(l)'
        return jacdotv
           
    def prec_setup(self, t, u, udot, r, alpha, data):
        '''
        to setup a preconditioner, which is the exact Jacobian
        '''
        self.stats['prec_setup'] += 1
        if self.verbose:
            print '*-prec_setup(e)'
            print '  at t =', t
        jacobian_csc = self.jac(alpha, t, u, udot, [k.user_parameters.values()[0] for k in self.ks])
        self.preconditioner = splu(jacobian_csc)
        if self.verbose:
            print '*-prec_setup(l)'
        return data

    def prec_solve(self, t, y, yp, r, rhs, c, delta, data):
        '''
        for the solutin of the preconditioner
        '''
        self.stats['prec_solve'] += 1
        if self.verbose:
            print '*--prec_solve(e)'
            print '*--prec_solve(l)'
        return self.preconditioner.solve(rhs)

    def update_time(self, t):
        '''
        to update time in time dependent expressions
        '''
        for f in self.fs:
            f.t = t
        for bc in self.bcs:
            bc.function_arg.t = t

    def print_stats(self):
        '''
        to print out numbers of times the callbacks are called by IDAS
        '''
        print "\n=== fendials stats ===\n"
        for key in sorted(self.stats.keys()):
            print "Number of %10s calls: %d " % (key, self.stats[key])