import dolfin
import numpy
from scipy.integrate import simps
from assimulo.problem import Implicit_Problem
from assimulo.solvers import IDA
from Callback import Callback
import matplotlib.pyplot as plt
import time
import os
import sys


class Problem(object):
    """
    FENDIALS interface to link DOLFIN to SUNDIALS
    """

    def __init__(self, u, F, fs, u0, T, bcs, dae, ks, verbose):
        """
        u  : DOLFIN Function object, i.e. Function(V)
        F  : UFL form of the right-hand-side, i.e. dot(u,v)*dx
        fs : array of time-dependent expressions, i.e. [Expression("2.0*x[0]*t"), t=0.0]
        u0 : initial condition, either an interplolated u or a DOLFIN expression is OK
        bcs: array of DirichletBCs, i.e. [DirichletBC(V, Constant('1.0'), boundary)]
        dae: array of indices of the differential part in a mixed function space
             In a single-space case, it's [0]; In a multi-space case (W=V*Q),
             if both parts are differential it's [0,1].
        ks : array of parameter-dependent expressions
        verbose: signal to print some extra info for debug purpose, False by default
        """
        self.u = u
        self.F = F
        self.fs = fs
        self.ks = ks
        self.u0 = u0
        self.T = T
        self.bcs = bcs
        # self.dotbcs = dotbcs
        self.dae = dae
        self.verbose = verbose
        self.set_default_parameters()

    def set_default_parameters(self):
        """
        to set defalt parameters
        """
        self.parameters = {}
        self.parameters['name'] = 'fendials'

        # parameters for implicit_problem
        PAR_imp = self.parameters['implicit_problem'] = {}
        PAR_imp['usejac'] = True
        PAR_imp['usejacv'] = True
        PAR_imp['useprec'] = True
        PAR_imp['calculate_yd0'] = False
        PAR_imp['yd0'] = []
        PAR_imp['make_consistent'] = True

        # parameters for ida solver
        PAR_ida = self.parameters['ida_solver'] = {}
        PAR_ida['atol'] = 1.0e-10
        PAR_ida['rtol'] = 1.0e-10
        PAR_ida['inith'] = self.T/1000.0
        PAR_ida['usejac'] = True
        PAR_ida['linear_solver'] = 'DENSE'
        PAR_ida['suppress_alg'] = True
        PAR_ida['report_continuously'] = True
        PAR_ida['verbosity'] = 30
        PAR_ida['backward'] = False
        PAR_ida['verbosity'] = 30
        PAR_ida['display_progress'] = True
        PAR_ida['lsoff'] = False
        PAR_ida['display_progress'] = True
        PAR_ida['maxh'] = 0
        PAR_ida['maxord'] = 5
        PAR_ida['maxsteps'] = 10000
        # self.parameters['ida_solver']['num_threads'] = 1
        PAR_ida['store_event_points'] = True
        PAR_ida['time_limit'] = 0
        PAR_ida['tout1'] = 0.001

        # parameter for output
        PAR_out = self.parameters['output'] = {}
        PAR_out['number_of_variables'] = self.u.vector().size()
        PAR_out['number_of_steps'] = 0
        PAR_out['stop_time'] = self.T
        PAR_out['mean_step_length'] = 0.0
        PAR_out['max_step_length'] = 0.0
        PAR_out['min_step_length'] = 0.0
        PAR_out['elapsed_time'] = 0.0

    def set_callback(self, u, F, fs, u0, bcs, dae, linear_solver, k, verbose):
        """
        to create Callback object
        """
        self.call_back = Callback(u, F, fs, u0, bcs, dae, linear_solver, k, verbose)

    def set_implicit_problem(self):
        """
        to create Assimulo implcit problem
        """
        self.parameters['name'] = self.parameters['name'] + ' [' + \
                    self.parameters['ida_solver']['linear_solver'] + ']'
        self.y0 = dolfin.interpolate(self.u0, self.u.function_space()).vector().array()
        # #--> to calculate a consistent yd0 manually
        # #--> Commented out for now, because IDA(S) is able to do it by itself.
        # #--> Left here for later experiments
        # if self.parameters['implicit_problem']['calculate_yd0']:
        #     rhs = dolfin.assemble(self.F)
        #     rhs[:] = 0.0
        #     for dotbc in self.dotbcs:
        #         dotbc.apply(rhs)
        #     udot2 = rhs.array()[self.call_back.isfixed_mask]
        #     self.yd0 = self.call_back.slopes(udot2)
        # elif len(self.parameters['implicit_problem']['yd0']) > 0:
        #     self.yd0 = self.parameters['implicit_problem']['yd0']
        # else:
        #     self.yd0 = 0.0*self.y0
        if len(self.parameters['implicit_problem']['yd0']) > 0:
            self.yd0 = self.parameters['implicit_problem']['yd0']
        else:
            self.yd0 = 0.0*self.y0
        self.p0 = [k.user_parameters.values()[0] for k in self.ks]
        self.imp_mod = Implicit_Problem(
            self.call_back.res, self.y0, self.yd0, p0=self.p0, name=self.parameters['name'])
        if self.parameters['implicit_problem']['usejac']:
            self.imp_mod.jac = self.call_back.jac
        if self.parameters['implicit_problem']['usejacv']:
            self.imp_mod.jacv = self.call_back.jacv
        if self.parameters['implicit_problem']['useprec']:
            self.imp_mod.prec_setup = self.call_back.prec_setup
            self.imp_mod.prec_solve = self.call_back.prec_solve

    def set_ida_solver(self):
        """
        to create IDA solver
        """
        self.imp_sim = IDA(self.imp_mod)
        self.imp_sim.pbar = self.p0
        self.imp_sim.atol = self.parameters['ida_solver']['atol']
        self.imp_sim.rtol = self.parameters['ida_solver']['rtol']
        self.imp_sim.inith = self.parameters['ida_solver']['inith']
        self.imp_sim.usejac = self.parameters['ida_solver']['usejac'] # Required for use of jacv
        self.imp_sim.linear_solver = self.parameters['ida_solver']['linear_solver']  # 'DENSE' | 'SPGMR'
        self.imp_sim.algvar = numpy.zeros(self.y0.size, dtype=float)
        for i in self.dae:
            self.imp_sim.algvar[self.call_back.sub_vertex_to_dof_map[i]] = 1.0
        self.imp_sim.algvar[self.call_back.isfixed_mask] = 0.0
        self.imp_sim.suppress_alg = self.parameters['ida_solver']['suppress_alg']
        self.imp_sim.report_continuously = self.parameters['ida_solver']['report_continuously']
        self.imp_sim.display_progress = self.parameters['ida_solver']['display_progress']
        if (not self.parameters['implicit_problem']['calculate_yd0']) and self.parameters['implicit_problem']['make_consistent']:
            # print 'enter make_consistent...........'
            self.imp_sim.make_consistent('IDA_YA_YDP_INIT')#IDA_YA_YDP_INIT
        self.imp_sim.verbosity = self.parameters['ida_solver']['verbosity']
        self.imp_sim.backward = self.parameters['ida_solver']['backward']
        self.imp_sim.display_progress =  self.parameters['ida_solver']['display_progress']
        self.imp_sim.lsoff = self.parameters['ida_solver']['lsoff']
        self.imp_sim.display_progress = self.parameters['ida_solver']['display_progress']
        self.imp_sim.maxh = self.parameters['ida_solver']['maxh']
        self.imp_sim.maxord = self.parameters['ida_solver']['maxord']
        self.imp_sim.maxsteps = self.parameters['ida_solver']['maxsteps']
        self.imp_sim.report_continuously = self.parameters['ida_solver']['report_continuously']
        self.imp_sim.store_event_points = self.parameters['ida_solver']['store_event_points']
        self.imp_sim.time_limit = self.parameters['ida_solver']['time_limit']
        self.imp_sim.tout1 = self.parameters['ida_solver']['tout1']

    def run(self):
        """
        to run simulation
        """
        self.set_callback(self.u, self.F, self.fs, self.u0, self.bcs, self.dae, self.parameters['ida_solver']['linear_solver'], self.ks, self.verbose)
        print '\n----1----set callback'
        self.set_implicit_problem()
        print '\n----2----set implicit problem'
        self.set_ida_solver()
        print '\n----3----set solver'
        tstart = time.time()
        print '\n----4----run simulation'
        self.t, self.y, self.yd = self.imp_sim.simulate(self.T, ncp=0)
        self.delta = numpy.diff(self.t)
        self.parameters['output']['elapsed_time'] = time.time() - tstart
        self.parameters['output']['number_of_steps'] = len(self.t) - 1
        self.parameters['output']['mean_step_length'] = numpy.mean(self.delta)
        self.parameters['output']['max_step_length'] = numpy.max(self.delta)
        self.parameters['output']['min_step_length'] = numpy.min(self.delta)

    def print_stats(self):
        """
        to print out information related to time-stepping
        """
        self.call_back.print_stats()
        print "\n=== solution statistics ===\n"
        print " Number of variables : %d"   % self.parameters['output']['number_of_variables']
        print "           Stop time : %.3e" % self.parameters['output']['stop_time']
        print "Number of time steps : %d"   % self.parameters['output']['number_of_steps']
        print "      Mean time step : %.3e" % self.parameters['output']['mean_step_length']
        print "       Max time step : %.3e" % self.parameters['output']['max_step_length']
        print "       Min time step : %.3e" % self.parameters['output']['min_step_length']
        print "        Elapsed time : %.3g sec" % self.parameters['output']['elapsed_time']

    def save_stats_figure(self, savefig=True):
        """
        to draw and save a figure on time-stepping
        """
        path = 'stats'
        abs_path = os.path.dirname(os.path.realpath(sys.argv[0]))
        stats_path = abs_path + '/' + path
        if not os.path.exists(stats_path):
            os.makedirs(stats_path)
        plt.figure()
        plt.semilogy(self.t[1:], self.delta, 'b.', label=r'$\Delta t$')
        plt.xlabel(r'$t$')
        plt.legend()
        fig_title = '%s $\Delta t$ (num of var: %d, num of steps: %d)'\
            % (self.parameters['name'], self.parameters['output']['number_of_variables'], self.parameters['output']['number_of_steps'])
        plt.title(fig_title)
        if savefig:
            plt.savefig(stats_path + '/stats.pdf')
        plt.show()

    def plot_stats(self):
        """
        to draw a figure on time-stepping
        """
        plt.semilogy(self.t[1:], self.delta, 'b.', label=r'$\Delta t$')
        plt.xlabel(r'$t$')
        plt.legend()
        fig_title = '%s $\Delta t$ (num of var: %d, num of steps: %d)'\
            % (self.parameters['name'], self.parameters['output']['number_of_variables'], self.parameters['output']['number_of_steps'])
        plt.title(fig_title)
        plt.show()

    def save_solution(self):
        """
        to save solution in local file, one file per time-step
        """
        path = 'solutions'
        abs_path = os.path.dirname(os.path.realpath(sys.argv[0]))
        slt_path = abs_path + '/' + path
        # to make a clear folder
        if not os.path.exists(slt_path):
            os.makedirs(slt_path)
        for the_file in os.listdir(slt_path):
            file_path = os.path.join(slt_path, the_file)
            try:
                if os.path.isfile(file_path):
                    os.unlink(file_path)
            except Exception, e:
                print e
        for i in range(len(self.t)):
            self.u.vector()[:] = self.y[i]
            solution = dolfin.File(slt_path + '/%.3e.xml' % self.t[i])
            solution << self.u

    def save_delta(self):
        """
        to save times in a local file
        """
        path = 'stats'
        abs_path = os.path.dirname(os.path.realpath(sys.argv[0]))
        stats_path = abs_path + '/' + path
        if not os.path.exists(stats_path):
            os.makedirs(stats_path)
        numpy.savetxt(stats_path + '/t.out', self.t, delimiter=',', fmt='%.3e')
        numpy.savetxt(
            stats_path + '/delta.out', self.delta, delimiter=',', fmt='%.3e')
        output = numpy.column_stack((numpy.asarray(self.t)[1:].flatten(), numpy.asarray(self.delta).flatten()))
        numpy.savetxt(stats_path + '/output.dat', output, delimiter=' ', fmt='%.3e')
        content=r'''\documentclass{standalone}
\usepackage[tikz,
border=1,
]{pgfplots}
\pgfplotsset{compat=1.5}
\begin{document}
\begin{tikzpicture}
\begin{semilogyaxis}[
title=%s,
width=10cm,
xlabel={$t$},
ylabel={$\Delta t$},
]
\addplot[only marks,red,mark size=1] table {output.dat};
\end{semilogyaxis}
\end{tikzpicture}
\end{document}
'''%self.parameters['name']
        f = open(stats_path + '/output.tex',"w")
        f.write(content)
        f.close()
    def plot_solution(self):
        if self.call_back.shape == 1:
            figs = [dolfin.plot(self.u, title='u')]
        else:
            figs = [dolfin.plot(self.u.split()[i], title='%d' % i)
                    for i in range(self.call_back.shape)]
        for i in range(len(self.y)):
            self.u.vector()[:] = self.y[i]
            for fig in figs:
                fig.plot()
        dolfin.interactive()

    def object_g(self, term):
        """
        to get ufl form of the objective functional
        """
        DG = dolfin.FunctionSpace(self.u.function_space().mesh(), 'DG', 0)
        omg = dolfin.TestFunction(DG)
        g = term*omg*dolfin.dx
        return g

    def do_sa(self, u, term, plot_sa = False):
        """
        to calculate 
        \frac{dg}{dp} = \frac{\partial g}{\partial u}\frac{\partial u}{\partial p}
                        + \frac{\partial g}{\partial p}
        and 
        $\frac{dG}{dp} = \int_{0}^{T}\frac{dg}{dp}dt$
        """
        g = self.object_g(term)
        du = dolfin.TrialFunction(self.u.function_space())
        J = dolfin.derivative(g, u, du)
        self.dgdp = numpy.zeros((len(self.t), len(self.ks)))
        self.dgdu = numpy.zeros((len(self.t), self.u.vector().array().size))
        self.dudp = numpy.zeros((len(self.t), self.u.vector().array().size, len(self.ks)))
        self.pdgdp = numpy.zeros((len(self.t), len(self.ks)))
        self.dGdp = numpy.zeros((len(self.t), len(self.ks)))
        pdgdp_ufl = [dolfin.diff(term, k) for k in self.ks]

        for i in range(len(self.t)):
            u.vector()[:] = self.y[i]
            M = dolfin.as_backend_type(dolfin.assemble(J)).sparray()
            # self.dgdu[i][:] = dolfin.project(dolfin.diff(g, u)).vector().array()
            self.dgdu[i][:] = M.sum(axis=0)
            for j in range(len(self.ks)):
                self.dudp[i,:,j] = numpy.asarray(self.imp_sim.p_sol)[j,i,:]
                self.pdgdp[i,j] = dolfin.assemble(pdgdp_ufl[j]*dolfin.dx(self.u.function_space().mesh()))
                self.dgdp[i,j] = numpy.dot(self.dgdu[i], self.dudp[i,:,j])+self.pdgdp[i,j]
                self.dGdp[i,j] = simps(self.dgdp[:i+1,j], self.t[:i+1])
        if plot_sa:
            for j in range(len(self.ks)):
                number_of_figures = 5
                # dudp
                plt.figure(int(j)*number_of_figures+1)
                for i in range(self.call_back.shape):
                    fig = plt.subplot('%d%d%d'%(self.call_back.shape,1,i))
                    plt.plot(self.t, self.dudp[:,self.call_back.sub_vertex_to_dof_map[i],int(j)])
                    fig.set_xlabel('t')
                    fig.set_ylabel('du[%d]dp'%int(i))
                plt.title('dudp')
                # dgdu
                plt.figure(int(j)*number_of_figures+2)
                for i in range(self.call_back.shape):
                    fig = plt.subplot('%d%d%d'%(self.call_back.shape,1,i))
                    plt.plot(self.t, self.dgdu[:,self.call_back.sub_vertex_to_dof_map[i]])
                    fig.set_xlabel('t')
                    fig.set_ylabel('dgdu[%d]'%int(i))
                plt.title('dgdu')
                # pdgdp
                plt.figure(int(j)*number_of_figures+3)
                plt.plot(self.t, self.pdgdp[:,int(j)])
                plt.title('pdgdp')
                # dgdp
                plt.figure(int(j)*number_of_figures+4)
                plt.plot(self.t, self.dgdp[:,int(j)])
                plt.title('dgdp')
                # dGdp
                plt.figure(int(j)*number_of_figures+5)
                plt.plot(self.t, self.dGdp[:,int(j)])
                plt.title('dGdp')
        plt.show()
        return [self.dgdp[-1], self.dGdp[-1]]
