try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'A FEniCS interface to SUNDIALS',
    'author': 'Jon Pearce and Chao Zhang',
    'url': 'https://bitbucket.org/seventy/fendials',
    'author_email': 'j.pearce@auckland.ac.nz and chao.zhang@auckland.ac.nz',
    'version': '0.1',
    'install_requires': [],
    'packages': ['fendials'],
    'scripts': [],
    'name': 'fendials'
}

setup(**config)