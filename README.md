# FENDIALS: a FEniCS interface to SUNDIALS

The package is developed as an interface between FEniCS/DOLFIN and SUNDIALS, and the aim is to make use of the matural functionalities of SUNDIALS, namely adaptive time-stepping strategies, nonlinear Newton-Krylov solvers and sensitivity analysis capabilities.

## Compatibility

The package is compatible with [FEniCS](http://fenicsproject.org/) 1.6.0, [SUNDIALS](http://computation.llnl.gov/projects/sundials-suite-nonlinear-differential-algebraic-equation-solvers) 2.6.2 and a *customized* [Assimulo](http://www.jmodelica.org/assimulo). We call it a customized Assimulo, which could be downloaded from [here](https://drive.google.com/file/d/0B-AASzRaTKkCNEFOak1ELUJVQ0k/view?usp=sharing), because we add some necessary callbacks to make use of preconditioned GMRES in IDA(S), (i.e. IDASpilsPrecSetupFn and IDASpilsPrecSolveFn), which seems not provided by the latest *official* Assimulo.

## How it works

FENDIALS works by feeding SUNDIALS with automatically generated callbacks for residual evaluation, for Jacobian evaluation or Jacobian-vector products, and for the setup and solution of Krylov preconditioners.

## How to use

It's relatively easy to use FENDIALS and there is no need to make much change to your original code. You'll find soon that everything is almost identical to a pure DOLFIN code. I'll convince you with a complete code on a transient heat diffusion problem:

```python
from dolfin import *
from fendials import Problem

parameters.linear_algebra_backend = 'Eigen'

nx = ny = 20
mesh = UnitSquareMesh(nx, ny)
degree = 1
V = FunctionSpace(mesh, 'Lagrange', degree)

u0 = Expression('t*(1.0-x[0])', t=0)
u_0 = interpolate(u0, V)

f = Expression("10*sin(pi/2*t)*exp(-((x[0]-0.7)*(x[0]-0.7) + (x[1]-0.5)*(x[1]-0.5))/0.01)", t=0)

class Boundary(SubDomain):  # define the Dirichlet boundary
    def inside(self, x, on_boundary):
        return on_boundary and ((x[0] < DOLFIN_EPS) or (1.0 - x[0] < DOLFIN_EPS) )

boundary = Boundary()
bcs = [DirichletBC(V, u0, boundary)]
u = Function(V, name='u')
v = TestFunction(V)
k = Expression('p', p=0.1)
F = f * v * dx - k*inner(nabla_grad(u), nabla_grad(v)) * dx

fs = [f]
T  = 1.0
ks = [k]
linear_solver = 'SPGMR' # or 'DENSE'
verbose = False

myProblem = Problem(u, F, fs, u0, T, bcs, [0], ks, verbose)
myProblem.parameters['ida_solver']['linear_solver'] = linear_solver
myProblem.parameters['ida_solver']['atol'] = 1.0e-7
myProblem.parameters['ida_solver']['rtol'] = 1.0e-7
myProblem.parameters['ida_solver']['inith'] = 1.0e-6
myProblem.run()
myProblem.print_stats()
# myProblem.save_solution(True)
# myProblem.save_delta()
myProblem.plot_stats()
myProblem.plot_solution()
plotSA = False
dgdp, dGdp = myProblem.do_sa(u, u, plotSA)
print 'Done!'
```

## Some comments

- The integration tolerances `atol` and `rtol` are very important as they are directly related to  nonlinear iteraton and the process of choosing step size. Somehow it's tricky to find a suitable pair, and it won't even able to find a consistent initial condition if you fail to find one.

- It's extracted partly from [Chao Zhang's](http://www.des.auckland.ac.nz/people/chao-zhang) PhD project on THM simulation in porous media, but the original idea is from [Jon Pearce](http://www.engineering.auckland.ac.nz/people/j-pearce).