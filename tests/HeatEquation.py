from dolfin import *
from fendials import Problem

parameters.linear_algebra_backend = 'Eigen'

nx = ny = 20
mesh = UnitSquareMesh(nx, ny)
degree = 1
V = FunctionSpace(mesh, 'Lagrange', degree)

u0 = Expression('t*(1.0-x[0])', t=0)
u_0 = interpolate(u0, V)
udot0 = Expression("1.0-x[0]", t=0)
f = Expression("10*sin(pi/2*t)*exp(-((x[0]-0.7)*(x[0]-0.7) + (x[1]-0.5)*(x[1]-0.5))/0.01)", t=0)

class Boundary(SubDomain):  # define the Dirichlet boundary
    def inside(self, x, on_boundary):
        return on_boundary and ((x[0] < DOLFIN_EPS) or (1.0 - x[0] < DOLFIN_EPS) )

boundary = Boundary()
bcs = [DirichletBC(V, u0, boundary)]
u = Function(V, name='u')
v = TestFunction(V)
k = Expression('p', p=0.1)
F = f * v * dx - k*inner(nabla_grad(u), nabla_grad(v)) * dx

fs = [f]
T  = 1.0
ks = [k]
linear_solver = 'SPGMR'
verbose = False

myProblem = Problem(u, F, fs, u0, T, bcs, [0], ks, verbose)
myProblem.parameters['ida_solver']['linear_solver'] = 'SPGMR' # SPGMR | DENSE
myProblem.parameters['ida_solver']['atol'] = 1.0e-7
myProblem.parameters['ida_solver']['rtol'] = 1.0e-7
myProblem.parameters['ida_solver']['inith'] = 1.0e-6
myProblem.run()
myProblem.print_stats()
# myProblem.save_solution(True)
# myProblem.save_delta()
myProblem.plot_stats()
myProblem.plot_solution()
plotSA = False
dgdp, dGdp = myProblem.do_sa(u, u, plotSA)
print 'Done!'