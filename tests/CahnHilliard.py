from dolfin import *
import random
from fendials import Problem

parameters.linear_algebra_backend = 'Eigen'

# Initial conditions
class InitialConditions(Expression):
  def __init__(self):
      random.seed(2 )
  def eval(self, values, x):
      values[0] = 0.63 + 0.02*(0.5 - random.random())
      values[1] = 0.0
  def value_shape(self):
      return (2,)

mesh = UnitIntervalMesh(100)
# mesh = UnitSquareMesh(49,49)
V   = FunctionSpace(mesh, "Lagrange", 1)
ME  = V*V

q,v = TestFunctions(ME)
u   = Function(ME)

u.interpolate(InitialConditions())

c,mu = split(u)
c = variable(c)
f    = 100*c**2*(1-c)**2
dfdc = diff(f, c)
# lmbda  = Constant(1.0e-02)
k = Expression('p', p = 1.0e-2)
F = -dot(grad(mu), grad(q))*dx + mu*v*dx - dfdc*v*dx - k*dot(grad(c), grad(v))*dx

T = 5e-5
verbose=False

myProblem = Problem(u, F, [], u, T, [], [0], [k], verbose)
myProblem.parameters['ida_solver']['linear_solver'] = 'SPGMR'
myProblem.parameters['ida_solver']['atol'] = 1.0e-7
myProblem.parameters['ida_solver']['rtol'] = 1.0e-7
myProblem.run()
myProblem.plot_stats()
myProblem.plot_solution()
dgdp, dGdp = myProblem.do_sa(u,c+mu)
print 'DONE!'