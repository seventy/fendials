from dolfin import *
from numpy import power,pi,sin
from fendials import Problem

parameters.linear_algebra_backend = 'Eigen'

class InitialConditions(Expression):
  def eval(self, values, x):
    if between(x[0],(0.75,1.25)) and between(x[1],(0.75,1.25)):
      values[1] = 0.25*power(sin(4*pi*x[0]),2)*power(sin(4*pi*x[1]),2)
      values[0] = 1 - 2*values[1]
    else:
      values[1] = 0
      values[0] = 1
  def value_shape(self):
    return (2,)

# Define mesh, function space and test functions
mesh = RectangleMesh(Point(0.0, 0.0), Point(2.0, 2.0), 49, 49)
V = FunctionSpace(mesh, "Lagrange", 1)
ME = V*V
q1,q2 = TestFunctions(ME)

# Define and interpolate initial condition
W = Function(ME)
W.interpolate(InitialConditions())
u,v = split(W)

# Define parameters in Gray-Scott model
# Du  = Constant(8.0e-5)
Du  = Expression('p', p = 8.0e-5)
Dv  = Constant(4.0e-5)
F   = Constant(0.024)
k   = Constant(0.06)

# Define the right hand side for each of the PDEs
F1 = (-Du*inner(grad(u),grad(q1)) - u*(v**2)*q1 + F*(1-u)*q1)*dx
F2 = (-Dv*inner(grad(v),grad(q2)) + u*(v**2)*q2 - (F+k)*v*q2)*dx

F = F1 + F2
T = 250.0
verbose=False

cal_slopes = False
linear_solver = 'SPGMR'
myProblem = Problem(W, F, [], InitialConditions(), T, [], [0,1], [Du], verbose)
myProblem.parameters['ida_solver']['linear_solver'] = 'SPGMR'
myProblem.parameters['implicit_problem']['calculate_yd0'] = False
myProblem.parameters['ida_solver']['atol'] = 1.0e-4
myProblem.parameters['ida_solver']['rtol'] = 1.0e-4
myProblem.parameters['ida_solver']['inith'] = 1.0e-6
myProblem.run()
myProblem.print_stats()
# myProblem.save_solution(True)
# myProblem.save_delta()
myProblem.plot_stats()
myProblem.plot_solution()
dgdp, dGdp = myProblem.do_sa(W,u+v)
print 'Done!'